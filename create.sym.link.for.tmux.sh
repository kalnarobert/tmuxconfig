#!/bin/bash

if [ $BACKED_UP_HOME="" ];then 
  BACKED_UP_HOME=$HOME/; 
fi
TMUXCONFIG=${BACKED_UP_HOME}config/tmux

# if you want to use an other directory, e.g. the project directory, then uncomment this:
# TMUXCONFIG=$( dirname `readlink -f "${BASH_SOURCE[0]}"` )

if [[ -f ~/.tmux.conf ]];then
  mv ~/.tmux.conf tmux.conf.bck
fi
ln -sf ${TMUXCONFIG}/tmux.conf ~/.tmux.conf
