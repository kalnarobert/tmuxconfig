#!/bin/bash

#export PS4="\$LINENO: "     # Prints out the line number being executed by debug
#set -xv                     # Turn on debugging

exists_another_pane_in_direction() {
    local direction=$1
    if [[ "$(xdotool getwindowfocus getwindowname)" != "tmux-kalnar" ]]; then
       echo "1"
       return 0
    fi
    panes=$(tmux list-panes -F '#{pane_top} #{pane_bottom} #{pane_left} #{pane_right} #{window_width} #{window_height} #{?pane_active,active,} ' | grep "active")
    

    if [[ $direction == "up" && $(echo $panes | cut -d" " -f1) == "0" ]]; then
      echo 1
    elif [[ $direction == "left" && $(echo $panes | cut -d" " -f3) == "0" ]]; then 
      echo 1
    elif [[ $direction == "down" && $(echo $panes | awk '$2+1!=$6') == "" ]]; then 
      echo 1
    elif [[ $direction == "right" && $(echo $panes | awk '$4+1!=$5') == "" ]]; then 
      echo 1
    else
      echo 0
    fi
}


dir=$1 #direction
mode=$2 #if value  = "tmux-only", navigation shortcuts won't apply in system's window scope
        # ....so this script won't be used and navigation will be handled by tmux only

if [ "$dir" = "up" ]
then
    tmuxArg="U"
    vimArg=K
elif [ "$dir" = "right" ]
then
    tmuxArg="R"
    vimArg=L
elif [ "$dir" = "down" ]
then
    tmuxArg="D"
    vimArg=J
elif [ "$dir" = "left" ]
then
    tmuxArg="L"
    vimArg=H
else
    echo -e "Invalid dirrection parameter supplied\nUsage: tmux_sys_win_aware_navigation up|right|down|left"
    exit 1
fi

exists=$(exists_another_pane_in_direction $dir)

if [[ $exists = '0' ]]
then
    command=$(tmux list-panes -F '#{pane_current_command} #{?pane_active,active,} ' | grep active | cut -d" " -f1)
    if [[ "$command" == "vim" ]]; then
      #tmux display-message "in vim"
      tmux send-keys C-${vimArg}
      # TODO nagivating to edge vim split
    else
      tmux select-pane -$tmuxArg
      #tmux focus $tmuxArg
    fi
else
    i3-msg -q focus $dir
fi
exit 0
#set +xv #end of debug
